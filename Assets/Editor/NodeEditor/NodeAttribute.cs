﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public class NodeAttribute : Attribute
{
    public string menuItem;
    public Type baseType;
    public NodeAttribute(string mi, Type t)
    {
        menuItem = mi;
        baseType = t;
    }
}
