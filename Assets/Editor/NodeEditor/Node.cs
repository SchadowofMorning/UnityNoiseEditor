﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
[Serializable]
public abstract class Node
{
    public Vector3 position;
    public Vector3 size;
    protected bool dragged = false;
    public abstract GUIContent content
    {
        get;
        protected set;
    }
    public void HandleEvents(Event e)
    {
        Rect rect = new Rect(position, size);
        switch (e.type)
        {
            case EventType.MouseDown:
                if (rect.Contains(e.mousePosition)) dragged = true;
                break;
            case EventType.MouseDrag:
                if (dragged)
                {
                    position += (Vector3)e.delta;
                    e.Use();
                }
                break;
            case EventType.MouseUp:
                dragged = false;
                break;
        }
    }
    public virtual void Draw()
    {
        Rect rect = new Rect(position, size);
        GUI.Box(rect, content);
    }
}
