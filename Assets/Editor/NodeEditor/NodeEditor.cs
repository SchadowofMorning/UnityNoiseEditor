﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Reflection;

public class NodeEditor : EditorWindow
{
    protected List<Node> nodes;
    protected Dictionary<string, Type> nodeTypes;
    protected GUIStyle boxStyle;
    #region Initialisation
    [MenuItem("Debugging/NodeEditor")]
    static void LoadWindow()
    {
        NodeEditor window = GetWindow<NodeEditor>();
        window.Init();
    }
    protected virtual void Init()
    {
        this.nodes = LoadCanvas();
        this.nodeTypes = resolveTypes(GetAllNodesOfCanvas<NodeEditor>());
        
    }
    protected Dictionary<string, Type> resolveTypes(IEnumerable<Tuple<string, Type>> input)
    {
        Dictionary<string, Type> result = new Dictionary<string, Type>();
        foreach (Tuple<string, Type> kvp in GetAllNodesOfCanvas<NodeEditor>())
        {
            result.Add(kvp.Item1, kvp.Item2);
        }
        return result;
    }
    #endregion
    #region Loading
    const string currentpath = "Assets/NodeCanvas";
    const string currentfile = "current.cnvs";
    protected List<Node> LoadCanvas()
    {
        List<Node> nodes = SpaceDatabase.LoadFromBinary<List<Node>>(currentpath, currentfile);
        if (nodes == null) nodes = new List<Node>();
        return nodes;
    }
    protected void SaveCanvas()
    {
        SpaceDatabase.WriteAsBinary(nodes, currentpath, currentfile);
    }
    #endregion
    #region Rendering
    protected virtual void OnGUI()
    {
        if (nodes == null) nodes = LoadCanvas();
        //ERRORPRONE SOLUTION
        if (nodeTypes == null) nodeTypes = resolveTypes(GetAllNodesOfCanvas<NodeEditor>());
        if (boxStyle == null) boxStyle = GUI.skin.box;
        foreach(Node n in nodes)
        {
            n.Draw();
        }
        HandleEvent(Event.current);
    }
    protected virtual void RenderGrid()
    {

    }
    protected virtual void ProcessContextMenu(Vector2 position)
    {
        GenericMenu menu = new GenericMenu();
        foreach(KeyValuePair<string, Type> kvp in nodeTypes)
        {
            menu.AddItem(new GUIContent(kvp.Key), false, () => nodes.Add(CreateNode(kvp.Value, position)));
        }
        menu.AddItem(new GUIContent("saving/save current"), false, () => SaveCanvas());
        menu.ShowAsContext();
    }
    #endregion
    #region EventHandling
    protected virtual void HandleEvent(Event e)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 1) ProcessContextMenu(e.mousePosition);
                break;
        }
        foreach (Node n in nodes) n.HandleEvents(e);
    }
    #endregion
    #region Reflection
    protected IEnumerable<Tuple<string, Type>> GetAllNodesOfCanvas<T>() where T : NodeEditor
    { 
        Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
        foreach(Assembly assembly in assemblys.Where(x => !x.GlobalAssemblyCache))
        {
            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
            {
                IEnumerable<NodeAttribute> attributes = type.GetCustomAttributes<NodeAttribute>();
                if(attributes.Count() > 0)
                {
                    NodeAttribute attr = attributes.First();
                    yield return new Tuple<string, Type>(attr.menuItem, type);
                }
            }
        }
    }
    protected Node CreateNode(Type t, Vector2 position)
    {
        object o = Activator.CreateInstance(t);
        try
        {
            Node n = (Node)o;
            n.position = position;
            return n;
        }
        catch (InvalidCastException e)
        {
            throw new InvalidCastException("Type is not of class Node");
        }
    }
    #endregion
}


