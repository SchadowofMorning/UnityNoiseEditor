﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public interface IMatrixCarrier
{
    int size { get; }
    float this[int x, int y]
    {
        get;
    }
    float[,] GetMatrix();
}