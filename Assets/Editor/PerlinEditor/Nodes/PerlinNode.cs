﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
[Node("Generation/Perlin", typeof(PerlinEditor)), Serializable]
public class PerlinNode : Node
{
    PerlinContainer container;
    private GUIContent preview;
    public override GUIContent content
    {
        get
        {
            return preview;
        }

        protected set
        {
            preview = value;
        }
    }
    public PerlinNode()
    {
        size = new Vector3(100, 100, 0);
        position = new Vector3(0, 0, 0);
        container = new PerlinContainer(0, 0, 0.05f, 0.05f, 64);
        preview = new GUIContent(container.texture);
    }
}

