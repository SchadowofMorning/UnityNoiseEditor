﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class PerlinContainer : MatrixContainer
{
    public PerlinContainer()
    {

    }
    public PerlinContainer(int xbase, int ybase, float xScale, float yScale, int size)
    {
        matrix = new float[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                float xval = (xbase + x) * xScale;
                float yval = (ybase + y) * yScale;
                float value = Mathf.PerlinNoise(xval, yval);
                matrix[x, y] = value;
            }
        }
    }
    
}

