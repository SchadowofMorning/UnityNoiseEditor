﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class PerlinEditor : NodeEditor {
    
    [MenuItem("ProjectSpace/PerlinEditor")]
    static void LoadWindow()
    {
        PerlinEditor window = GetWindow<PerlinEditor>();
        window.Init();
    }
    protected override void Init()
    {
        this.nodes = new List<Node>();
        this.nodeTypes = resolveTypes(GetAllNodesOfCanvas<PerlinEditor>());
    }
}
