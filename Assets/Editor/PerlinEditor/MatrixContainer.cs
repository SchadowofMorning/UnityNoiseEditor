﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MatrixContainer : ISDBWritable, IMatrixCarrier
{
    protected float[,] matrix;
    public int size
    {
        get
        {
            return matrix.GetUpperBound(0);
        }
    }
    protected string id;
    public string fileName
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }
    public string fileType
    {
        get
        {
            return "matrix";
        }
    }

    public float this[int x, int y]
    {
        get
        {
            return matrix[x, y];
        }
    }

    public float[,] GetMatrix()
    {
        return matrix;
    }
    public MatrixContainer()
    {

    }
    public MatrixContainer(float[,] input)
    {
        if (input.GetUpperBound(0) != input.GetUpperBound(1)) throw new InvalidOperationException("Matrixes for perlin noise need to be of Identity");
        matrix = input;
        
    }
    public Texture2D texture
    {
        get
        {
            Texture2D _tex = new Texture2D(size, size);
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    float f = matrix[x, y];
                    Color color = new Color(f, f, f);
                    _tex.SetPixel(x, y, color);
                }
            }
            _tex.Apply();
            return _tex;
        }
    }
}

