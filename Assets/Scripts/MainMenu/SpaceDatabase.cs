﻿
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using UnityEngine;
using System.Runtime.Serialization;

public static class SpaceDatabase
{
    public static void WriteAsBinary<T>(T data) where T : ISDBWritable
    {
        if (!Directory.Exists("GameData")) Directory.CreateDirectory("GameData");
        using (Stream stream = File.Open("GameData/" + typeof(T).ToString() + "/" + data.fileName + "." + data.fileType, FileMode.OpenOrCreate))
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.SurrogateSelector = createSurrogate();
            bf.Serialize(stream, data);
        }
    }
    public static void WriteAsBinary<T>(T data, string path, string name)
    {
        if (!Directory.Exists("GameData")) Directory.CreateDirectory("GameData");
        using (Stream stream = File.Open(Path.Combine(path, name), FileMode.OpenOrCreate))
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.SurrogateSelector = createSurrogate();
            bf.Serialize(stream, data);
        }
    }
    public static T LoadFromBinary<T>(string path, string name)
    {
        if (!Directory.Exists(path)) Directory.CreateDirectory(path);
        try
        {
            using (Stream stream = File.Open(Path.Combine(path, name), FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.SurrogateSelector = createSurrogate();
                return (T)bf.Deserialize(stream);
            }
        } catch(Exception e)
        {
            return default(T);
        }
    }
    public static T[] LoadAllOfType<T>() where T : ISDBWritable
    {
        string folder = "GameData/" + typeof(T).ToString();
        string[] itemstrings = Directory.GetFiles(folder);
        List<T> items = new List<T>();
        foreach(string s in itemstrings)
        {
            items.Add(LoadFromBinary<T>(folder, s));
        }
        return items.ToArray();
    }
    private static SurrogateSelector createSurrogate()
    {
        SurrogateSelector selector = new SurrogateSelector();
        selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new Vector3SerializationSurrogate());
        return selector;
    }
}
public interface ISDBWritable
{
    string fileName { get; set; }
    string fileType { get; }
}
