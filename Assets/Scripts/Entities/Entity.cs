﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Entity : MonoBehaviour {
    
    public abstract PlayableClass Class { get; }
    public abstract int MaxHealth { get; }
    #region Main Stats
    public abstract int Strength { get; }
    public abstract int Agility { get; }
    public abstract int Itellect { get; }
    #endregion
    #region Secondary Stats
    public abstract int Wisdom { get; }
    public abstract int Dirability { get; }
    public abstract int Dexterity { get; }
    #endregion
    #region Utility Stats
    public abstract int Speed { get; }
    public abstract int MagicalRediction { get; }
    public abstract int PhysicalReduction { get; }
    #endregion
}
