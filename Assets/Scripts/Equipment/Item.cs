﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public abstract string Id { get; }
    public abstract Sprite InventoryGraphic { get; }
    public abstract string SlotType { get; }

}
