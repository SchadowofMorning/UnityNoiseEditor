﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gear : Item {
    #region Main Stats
    public abstract int Strength { get; }
    public abstract int Agility { get; }
    public abstract int Itellect { get; }
    #endregion
    #region Secondary Stats
    public abstract int Wisdom { get; }
    public abstract int Dirability { get; }
    public abstract int Dexterity { get; }
    #endregion
    #region  Utility Stats
    public abstract int Speed { get; }
    public abstract int MagicalReduction { get; }
    public abstract int PhysicalReduction { get; }
    public abstract int Armor { get; }
    #endregion
    public abstract Slot EquipSlot { get; }
    public abstract GearType EquipType { get; }
    public abstract Rarity RareLvl { get; } 
    public abstract void OnEquip();
    
}
#region Enums
public enum Slot
{
    Head = 1,
    Amulet = 2,
    Shoulders = 3,
    Chest = 4,
    Waist = 5,
    Legs = 6,
    Feet = 7,
    RingR = 8,
    RingL = 9,
    Trinket1 = 10,
    Trinket2 = 11,
    MainHand = 12,
    OffHand = 13
}
public enum GearType
{
    LightArmor = 1,
    NormalArmor = 2,
    HeavyArmor = 3,
    Weapon = 4,
    Jewelery = 5
}
public enum Rarity
{
    Common = 1,
    Magical = 2,
    Rare = 3,
    Unique = 4
}
#endregion 