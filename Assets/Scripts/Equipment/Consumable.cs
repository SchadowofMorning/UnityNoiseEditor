﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumable : Item {

	public abstract int MaxCharges { get; }
    public abstract bool IsCombat { get; }
    public abstract Target onWho { get; }

    public abstract void OnUse(Character target);
}
public enum Target
{
    Self = 1,
    Friendly = 2,
    Hostile = 3
}