﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Field : MonoBehaviour
{
    public Coordinate coordinate;
    public FieldType type;
    public MapObject mapObject;
    public static Field Place(int x, int y, int z, FieldType t)
    {
        Field template = Resources.Load<Field>("World/Fields/" + t.ToString());
        Field field = Instantiate(template);
        field.coordinate = new Coordinate(x, y, z);
        field.transform.position = field.coordinate.vector;
        return field;
    }
    public static Field Place(Coordinate parentCoordinate, FieldData f)
    {
        Field template = Resources.Load<Field>("World/Fields/" + f.type.ToString());
        Field field = Instantiate(template);
        field.coordinate = parentCoordinate + f.coordinate;
        field.transform.position = field.coordinate.vector;
        return field;
    }
}

[Serializable]
public enum FieldType
{
    Planes = 1,
    Desert = 2,
    Water = 3,
    Forest = 4,
    Mountain = 5
}