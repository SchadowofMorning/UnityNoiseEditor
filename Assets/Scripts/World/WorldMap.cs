﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class WorldMap : MonoBehaviour, IMapCarrier
{
    [SerializeField]
    private int xSize;
    [SerializeField]
    private int zSize;
    [SerializeField]
    private float perlinScale;
    [SerializeField]
    private int chunkSize;
    private Dictionary<Coordinate, ChunkData> chunks;
    private Chunk current;
    private Coordinate currentView;
    [SerializeField]
    private new Camera camera;
    [SerializeField]
    private Vector3 cameraPosition;
    private ChunkData GenerateChunk(int xpos, int zpos)
    {
        FieldData[,] data = new FieldData[chunkSize, chunkSize];
        for (int x = 0; x < chunkSize; x++)
        {
            for (int z = 0; z < chunkSize; z++)
            {
                float xval = ((xpos * chunkSize) + x) * perlinScale;
                float zval = ((zpos * chunkSize) + z) * perlinScale;
                float val = Mathf.PerlinNoise(xval, zval);
                data[x, z] = FieldData.GetByElevation(new Coordinate(x, 0, z), val);
            }
        }
        return new ChunkData(xpos, 0, zpos, data);
    }

    private IEnumerator Start()
    {
        currentView = Coordinate.Zero;
        chunks = new Dictionary<Coordinate, ChunkData>();
        for (int x = 0; x < xSize; x++)
        {
            for (int z = 0; z < zSize; z++)
            {
                Coordinate coordinate = new Coordinate(x, 0, z);
                chunks[coordinate] = GenerateChunk(x, z);
                
                yield return new WaitForFixedUpdate();
            }
        }
        current = Chunk.CreateChunk(chunks[currentView]);
        current.InitializeFields();
        camera.transform.position = cameraPosition;
        camera.transform.rotation = Quaternion.Euler(90, 0, 0);
    }
    private void ChangeView(Coordinate move)
    {
        chunks[currentView] = new ChunkData(current);
        currentView += move;
        GameObject.Destroy(current.gameObject);
        current = Chunk.CreateChunk(chunks[currentView]);
        current.InitializeFields();
    }
    public void MoveUp()
    {
        ChangeView(Coordinate.North);
    }
    public void MoveDown()
    {
        ChangeView(Coordinate.South);
    }
    public void MoveLeft()
    {
        ChangeView(Coordinate.West);
    }
    public void MoveRight()
    {
        ChangeView(Coordinate.East);
    }
}

