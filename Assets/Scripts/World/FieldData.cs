﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
[Serializable]
public class FieldData
{
    public Coordinate coordinate;
    public FieldType type;
    public MapObject mapObject;
    public FieldData(Coordinate c, FieldType t)
    {
        coordinate = c;
        type = t;
    }
    public FieldData(Coordinate c, FieldType t,MapObject mO)
    {
        coordinate = c;
        type = t;
        mapObject = mO;
    }
    public FieldData(Field f)
    {
        coordinate = f.coordinate;
        type = f.type;
        mapObject = f.mapObject;
    }
    public static FieldData GetByElevation(Coordinate c, float height)
    {
        if(height < 0.2f)
        {
            return new FieldData(c, FieldType.Water);
        } else if(height < 0.4)
        {
            return new FieldData(c, FieldType.Desert);
        }
        else if (height < 0.6)
        {
            return new FieldData(c, FieldType.Planes);
        }
        else if (height < 0.8)
        {
            return new FieldData(c, FieldType.Forest);
        }
        else
        {
            return new FieldData(c, FieldType.Mountain);
        }
    }
    
}

