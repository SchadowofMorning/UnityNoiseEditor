﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public struct ChunkData : IMapCarrier
{
    public int size { get; private set; } 
    public Coordinate coordinate;
    public FieldData[,] content { get; private set; }
    public ChunkData(int x, int y, int z, FieldData[,] c)
    {
        if(c.GetUpperBound(0) != c.GetUpperBound(1)) throw new InvalidOperationException("Chunks need equal size");
        size = c.GetUpperBound(0);
        content = c;
        coordinate = new Coordinate(x, y, z);
        content = c;
    }
    public ChunkData(Chunk chunk)
    {
        size = chunk.template.size;
        coordinate = chunk.coordinate;
        content = new FieldData[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                content[x, z] = new FieldData(chunk.fields[x, z]);
            }
        }
    }
}

