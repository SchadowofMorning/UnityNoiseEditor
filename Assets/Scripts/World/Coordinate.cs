﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[Serializable]
public struct Coordinate
{
    const float hexScaleX = 1.74f;
    const float hexScaleZ = 1.5f;
    public int x;
    public int y;
    public int z;
    public Vector3 vector
    {
        get
        {
            float vx = x * hexScaleX + (z % 2 == 0 ? 0 : 0.87f);
            float vz = z * hexScaleZ;
            return new Vector3(vx, y, vz);
        }
    }
    public Coordinate(int X, int Y, int Z)
    {
        x = X;
        y = Y;
        z = Z;
    }
    public static Coordinate North { get { return new Coordinate(0, 0, 1); } }
    public static Coordinate South { get { return new Coordinate(0, 0, -1); } }
    public static Coordinate West { get { return new Coordinate(-1, 0, 0); } }
    public static Coordinate East { get { return new Coordinate(1, 0, 0); } }
    public static Coordinate Up { get { return new Coordinate(0, 1, 0); } }
    public static Coordinate Down { get { return new Coordinate(0, -1, 0); } }
    public static Coordinate Zero { get { return new Coordinate(0, 0, 0); } }
    public static Coordinate[] StandardDistance
    {
        get
        {
            return new Coordinate[] { North, South, West, East };
        }
    }

    public static Coordinate VecToCoordinate(Vector3 input)
    {
        int x = (int)Math.Round(input.x);
        int y = (int)Math.Round(input.y);
        int z = (int)Math.Round(input.z);
        return new Coordinate(x, y, z);
    }
    public float Distance(Coordinate target)
    {
        return Mathf.Sqrt(Mathf.Pow(target.x - this.x, 2) + Mathf.Pow(target.y - this.y, 2) + Mathf.Pow(target.z - this.z, 2));
    }
    public static bool operator ==(Coordinate c1, Coordinate c2)
    {
        return (c1.x == c2.x && c1.y == c2.y && c1.z == c2.z);
    }
    public static bool operator !=(Coordinate c1, Coordinate c2)
    {
        return !(c1.x == c2.x && c1.y == c2.y && c1.z == c2.z);
    }
    public static bool operator <(Coordinate c1, Coordinate c2)
    {
        return ((c1.x + c1.y + c1.z) < (c2.x + c2.y + c2.z));
    }
    public static bool operator >(Coordinate c1, Coordinate c2)
    {
        return ((c1.x + c1.y + c1.z) > (c2.x + c2.y + c2.z));
    }
    public static Coordinate operator +(Coordinate c1, Coordinate c2)
    {
        return new Coordinate(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z);
    }
    public static Coordinate operator -(Coordinate c1, Coordinate c2)
    {
        return new Coordinate(c1.x - c2.x, c1.y - c2.y, c1.z - c2.z);
    }
    public static Coordinate operator *(Coordinate c1, Coordinate c2)
    {
        return new Coordinate(c1.x * c2.x, c1.y * c2.y, c1.z* c2.z);
    }
    public static Coordinate operator *(Coordinate c, int i)
    {
        return new Coordinate(c.x * i, c.y * i, c.z * i);
    }
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override string ToString()
    {
        return ("[" + x.ToString() + ":" + y.ToString() + ":" + z.ToString() + "]");
    }
}

