﻿using UnityEngine;
using System.Collections;

public class Chunk : MonoBehaviour
{
    public ChunkData template;
    public Field[,] fields;
    [HideInInspector]
    public Coordinate coordinate;
    // Use this for initialization
    public static Chunk CreateChunk(ChunkData data)
    {
        Chunk template = Resources.Load<Chunk>("World/Chunk");
        Chunk instance = Instantiate(template);
        instance.template = data;
        instance.coordinate = data.coordinate;
        return instance;
    }
    public void InitializeFields()
    {
        fields = new Field[template.size, template.size];
        for (int x = 0; x < template.size; x++)
        {
            for (int z = 0; z < template.size; z++)
            {
                fields[x, z] = Field.Place(template.coordinate, template.content[x,z]);
                fields[x, z].transform.parent = transform;
            }
        }
    }
}
